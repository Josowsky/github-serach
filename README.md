## React app

### Setup

#### Install dependencies

```sh
yarn install
```

#### To start the app locally:

```sh
yarn start
```

**App will run at localhost:3000**

#### To create a production build:

```sh
yarn run build
```

### Project setup and tech stack

For this project I went with TypeScript.\
I used my react starter pack to bootstrap this project. [https://github.com/Josowsky/react-starter-pack](https://github.com/Josowsky/react-starter-pack)

App is live at: [https://tooploox-github-serach.netlify.com/](https://tooploox-github-serach.netlify.com/)

### Styling

For styling I'm using styled-components.\
Styled-components is a CSS-in-JS library which allows writing styles as react components.\
Styles are injected at runtime (when styled component is rendered into the DOM, library will insert its CSS code into the `head`).\
Normally, for large react project I would think twice about using CSS-in-JS as we don't yet have any well documented data about performance at large scale. Sometimes it makes more sense to go with CSS-modules.
Here, I decided to go this path because project is small.

### Router

React router was part of my starter pack.\
At the moment I need only one page so it doesn't makes sense to have a router, but I got an info that on the next stage of the recruitment I might return to this project so I decided to leave it here.\
In a commercial project, we would like to remove unused libraries e.g. router.

### User list limit

When you type something to the search input then GitHub api will return list of users that match the criteria.

Depending on the content of the input, this list might be very long. Normally for such a long result, we would like to implement pagination (e.g. infinite scroll). I wanted to keep this project simple so I just limited the results to 5 users. If you want to find a particular user, you should type exactly his user name.

### Structure

Components are grouped into two categories: pages and ui.
Pages components are top-level containers that hold all of the logic. You could treat them as controllers.
UI components are dumb, their role is just displaying the UI.

### 403 on the API

API sometimes returns 403 error due to small requests per minute limit. It could be improved by token authorization.\
I decided to keep my requests unauthorized to keep this project simple.

### Error handling

Sometimes API might return errors. I made just a simple `.catch` to catch errors and I'm logging them to the console:

```js
.catch(error => console.log(error));
```

In commercial application you would like to create some way to show errors to the user instead of logging them to the console. Initially I wanted to create some notifications component but I decided not to do this, to keep this project simple.

### Descriptions

I wasn't able to find user description anywhere in the API documentation so I didn't show it.
[https://developer.github.com/v3/search/#search-users](https://developer.github.com/v3/search/#search-users)
