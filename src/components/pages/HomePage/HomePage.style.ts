import styled from 'styled-components';

import { Loader } from 'components/ui/Loader/Loader';
import { TextInput } from 'components/ui/TextInput/TextInput';

export const StyledContainer = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const StyledTextInput = styled(TextInput)`
  width: 100%;
  max-width: 300px;
  margin-top: 20px;
`;

export const StyledLoader = styled(Loader)`
  margin-top: 50px;
`;

export const StyledCardGrid = styled.div<{
  isLoading?: boolean;
}>`
  margin-top: 55px;
  display: flex;
  flex-direction: column;
  align-items: center;
  opacity: ${({ isLoading }) => (isLoading ? 0.3 : 1)};
  pointer-events: ${({ isLoading }) => (isLoading ? 'none' : 'auto')};
`;

export const StyledCardContainer = styled.div`
  margin-bottom: 25px;
`;
