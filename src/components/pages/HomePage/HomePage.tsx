import React, { FunctionComponent, useState, useEffect, useRef } from 'react';
import debounce from 'lodash.debounce';

import { User } from 'types/models/User';
import { fetchUsers } from 'services/UserService/UserService';
import { UserCard } from 'components/ui/UserCard/UserCard';

import { StyledContainer, StyledTextInput, StyledLoader, StyledCardGrid, StyledCardContainer } from './HomePage.style';

const HomePage: FunctionComponent = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [users, setUsers] = useState<User[]>([]);
  const [isFetchingUsers, setIsFetchingUsers] = useState(false);

  const debouncedFetchUsers = useRef(
    debounce((searchQuery: string) => {
      setIsFetchingUsers(true);
      fetchUsers({ username: searchQuery })
        .then(fetchedUsers => setUsers(fetchedUsers.slice(0, 5)))
        .catch(error => console.log(error))
        .finally(() => setIsFetchingUsers(false));
    }, 800),
  );

  useEffect(() => {
    if (searchQuery !== '' && !isFetchingUsers) debouncedFetchUsers.current(searchQuery);
    if (searchQuery === '') setUsers([]);
  }, [searchQuery]);

  /**
   * Show loader component only when grid is empty.
   * Grid has its own loader.
   */
  const isLoaderVisible = isFetchingUsers && users.length === 0;
  /**
   * Grid has its own build in loader. It shows up when list of users in not empty.
   */
  const isUsersGridLoading = isFetchingUsers && users.length > 0;

  return (
    <StyledContainer>
      <StyledTextInput
        placeholder="Type Github username"
        label="Search Github user"
        value={searchQuery}
        onChange={setSearchQuery}
      />
      {isLoaderVisible && <StyledLoader />}
      <StyledCardGrid isLoading={isUsersGridLoading}>
        {users &&
          users.map(user => (
            <StyledCardContainer key={user.id}>
              <UserCard user={user} />
            </StyledCardContainer>
          ))}
      </StyledCardGrid>
    </StyledContainer>
  );
};

export { HomePage };
