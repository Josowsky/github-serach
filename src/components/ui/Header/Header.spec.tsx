import React from 'react';
import { mount } from 'enzyme';

import { Header } from './Header';

describe('Header', () => {
  describe('<Header />', () => {
    const header = mount(<Header />);

    test('renders correctly', () => {
      expect(header.find(Header)).toHaveLength(1);
      expect(header.find(Header).text()).toEqual('GitHub Search');
    });
  });
});
