import styled from 'styled-components';

export const StyledContainer = styled.header`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  width: 100%;
  height: 100%;
  padding: 20px 50px;
  background-color: #fff;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12);
  z-index: 2;
`;

export const StyledLogoContainer = styled.div`
  margin-right: auto;
  text-transform: uppercase;
`;
