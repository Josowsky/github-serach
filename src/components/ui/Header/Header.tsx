import React, { FunctionComponent } from 'react';

import { StyledContainer, StyledLogoContainer } from './Header.style';

interface HeaderProps {
  className?: string;
}

const Header: FunctionComponent<HeaderProps> = ({ className = '' }) => (
  <div className={className}>
    <StyledContainer>
      <StyledLogoContainer>
        <i>GitHub Search</i>
      </StyledLogoContainer>
    </StyledContainer>
  </div>
);

export { Header };
