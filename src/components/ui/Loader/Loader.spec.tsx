import React from 'react';
import { mount } from 'enzyme';

import { Loader } from './Loader';

describe('Loader', () => {
  describe('<Header />', () => {
    const loader = mount(<Loader />);

    test('renders correctly', () => {
      expect(loader.find(Loader)).toHaveLength(1);
      expect(loader.find(Loader).text()).toEqual('Loading...');
    });
  });
});
