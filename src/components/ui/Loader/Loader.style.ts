import styled from 'styled-components';

import { accentColor } from 'shared/constants/constants.style';

export const StyledContainer = styled.header`
  color: ${accentColor};
  font-weight: 600;
`;
