import React, { FunctionComponent } from 'react';

import { StyledContainer } from './Loader.style';

interface LoaderProps {
  className?: string;
}

const Loader: FunctionComponent<LoaderProps> = ({ className = '' }) => (
  <div className={className}>
    <StyledContainer>Loading...</StyledContainer>
  </div>
);

export { Loader };
