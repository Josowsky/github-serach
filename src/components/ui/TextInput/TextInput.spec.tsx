import React from 'react';
import { mount } from 'enzyme';

import { TextInput } from './TextInput';
import { StyledInput } from './TextInput.style';

describe('TextInput', () => {
  describe('<TextInput />', () => {
    const onChangeCallback = jest.fn();

    const props = {
      value: 'some value',
      label: 'some label',
      placeholder: 'some placeholder',
      onChange: onChangeCallback,
    };

    const textInput = mount(<TextInput {...props} />);

    test('renders correctly', () => {
      expect(textInput.find(TextInput)).toHaveLength(1);
      expect(textInput.find(TextInput).props()).toMatchObject(props);
      expect(textInput.find(StyledInput).prop('aria-label')).toEqual(props.label);
    });

    test('calls onChange with correct value', () => {
      textInput.find(StyledInput).simulate('change', { target: { value: 'Josowsky' } });
      expect(onChangeCallback).toHaveBeenCalledTimes(1);
      expect(onChangeCallback).toHaveBeenCalledWith('Josowsky');
    });
  });
});
