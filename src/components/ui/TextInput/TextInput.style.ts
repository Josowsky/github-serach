import styled from 'styled-components';

import { accentColor2, greyLight } from 'shared/constants/constants.style';

export const StyledInput = styled.input.attrs({
  type: 'text',
})`
  background: #fff;
  padding: 12px 20px;
  font-size: 15px;
  border-radius: 20px;
  color: ${accentColor2};
  width: 100%;
  border: 1px solid ${greyLight};
`;
