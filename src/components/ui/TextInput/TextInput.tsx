import React, { ChangeEvent, FunctionComponent } from 'react';

import { StyledInput } from './TextInput.style';

interface TextInputProps {
  className?: string;
  value: string;
  onChange: (value: string) => void;
  placeholder?: string;
  label?: string;
}

const TextInput: FunctionComponent<TextInputProps> = ({ value, onChange, placeholder, label, className }) => (
  <div className={className}>
    <StyledInput
      aria-label={label}
      placeholder={placeholder}
      value={value}
      onChange={(event: ChangeEvent<HTMLInputElement>) => onChange(event.target.value)}
    />
  </div>
);

export { TextInput };
