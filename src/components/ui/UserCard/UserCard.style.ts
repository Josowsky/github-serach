import styled from 'styled-components';

import { accentColor, accentColor2, accentColor3 } from 'shared/constants/constants.style';

export const StyledContainer = styled.section`
  background-color: #fff;
  width: 350px;
  border-radius: 4px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  padding: 30px 25px;
`;

export const StyledContent = styled.div`
  display: flex;
  align-items: flex-start;
  margin-bottom: 30px;
`;

export const StyledAvatar = styled.img`
  border-radius: 4px;
  width: 90px;
  margin-right: 25px;
`;

export const StyledName = styled.h1`
  font-size: 16px;
  font-weight: 600;
  color: ${accentColor2};
`;

export const StyledReposHeader = styled.button`
  color: ${accentColor};
  cursor: pointer;
  font-weight: 500;
`;

export const StyledReposLinkContainer = styled.div`
  margin-top: 15px;
`;

export const StyledRepoLink = styled.a.attrs({
  target: '_blank',
})`
  display: block;
  color: ${accentColor3};

  &:not(:last-child) {
    margin-bottom: 15px;
  }
`;
