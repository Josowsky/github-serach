import React, { FunctionComponent, useState, useEffect } from 'react';

import { User } from 'types/models/User';
import { Repository } from 'types/models/Repository';
import { fetchRepositories } from 'services/RepositoryService/RepositoryService';

import { UserCardUI } from './UserCardUI';

interface UserCardProps {
  user: User;
}

const UserCard: FunctionComponent<UserCardProps> = ({ user }) => {
  const [isReposDropdownOpened, setIsReposDropdownOpened] = useState(false);
  const [repositories, setRepositories] = useState<Repository[]>([]);
  const [isFetchingRepositories, setIsFetchingRepositories] = useState(false);

  useEffect(() => {
    if (isReposDropdownOpened && repositories.length === 0 && !isFetchingRepositories) {
      setIsFetchingRepositories(true);
      fetchRepositories({ username: user.login })
        .then(fetchedRepositories => setRepositories(fetchedRepositories.slice(0, 3)))
        .catch(error => console.log(error))
        .finally(() => setIsFetchingRepositories(false));
    }
  }, [isReposDropdownOpened]);

  const toggleReposDropdown = () => setIsReposDropdownOpened(isDropdownOpened => !isDropdownOpened);

  return (
    <UserCardUI
      isFetchingRepositories={isFetchingRepositories}
      isReposDropdownOpened={isReposDropdownOpened}
      toggleReposDropdown={toggleReposDropdown}
      user={user}
      repositories={repositories}
    />
  );
};

export { UserCard };
