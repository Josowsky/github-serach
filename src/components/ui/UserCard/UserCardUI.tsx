import React, { FunctionComponent } from 'react';

import { User } from 'types/models/User';
import { Repository } from 'types/models/Repository';

import { Loader } from 'components/ui/Loader/Loader';

import {
  StyledContainer,
  StyledContent,
  StyledAvatar,
  StyledName,
  StyledReposHeader,
  StyledReposLinkContainer,
  StyledRepoLink,
} from './UserCard.style';

interface UserCardUIProps {
  isFetchingRepositories: boolean;
  isReposDropdownOpened: boolean;
  user: User;
  toggleReposDropdown: () => void;
  repositories: Repository[];
}

const UserCardUI: FunctionComponent<UserCardUIProps> = ({
  isFetchingRepositories,
  isReposDropdownOpened,
  toggleReposDropdown,
  user,
  repositories,
}) => (
  <StyledContainer>
    <StyledContent>
      <StyledAvatar src={user.avatar_url} alt={`${user.login} avatar image`} />
      <StyledName>{user.login}</StyledName>
    </StyledContent>
    <StyledReposHeader onClick={toggleReposDropdown}>
      Repositories (click to {isReposDropdownOpened ? 'hide' : 'show'})
    </StyledReposHeader>
    {isReposDropdownOpened && (
      <StyledReposLinkContainer>
        {isFetchingRepositories && <Loader />}
        {repositories.map(repository => (
          <StyledRepoLink key={repository.id} title={repository.full_name} href={repository.html_url}>
            {repository.full_name}
          </StyledRepoLink>
        ))}
      </StyledReposLinkContainer>
    )}
  </StyledContainer>
);

export { UserCardUI };
