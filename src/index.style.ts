import styled, { createGlobalStyle } from 'styled-components';

import { Header } from 'components/ui/Header/Header';

import { accentColor4 } from './shared/constants/constants.style';

export const GlobalStyles = createGlobalStyle`
  html,
  body {
    margin: 0;
    padding: 0;
  }
  html {
    box-sizing: border-box;
  }
  *,
  *:before,
  *:after {
    box-sizing: inherit;
  }
  body {
    min-height: 100vh;
    font-family: -apple-system, BlinkMacSystemFont, Helvetica Neue, Helvetica, Arial, sans-serif;
    font-style: normal;
    font-variant: normal;
    background-color: ${accentColor4};
  }
  a {
    text-decoration: none;
    color: inherit;
  }
  button {
    border: 0;
    font-size: 16px;
    padding: 0;
  }
`;

export const StyledContainer = styled.div`
  width: 100vw;
  height: 100vh;
`;

export const StyledHeader = styled(Header)`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 70px;
`;

export const StyledContent = styled.div`
  padding-top: 70px;
  height: 100vh;
  width: 100vw;
`;
