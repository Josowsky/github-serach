import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { paths } from './shared/constants/constants.routes';

import { HomePage } from 'components/pages/HomePage/HomePage';

import { StyledContainer, StyledHeader, StyledContent, GlobalStyles } from './index.style';

ReactDOM.render(
  <Router>
    <GlobalStyles />
    <StyledContainer>
      <StyledHeader />
      <StyledContent>
        <Route exact path={paths.home}>
          <HomePage />
        </Route>
      </StyledContent>
    </StyledContainer>
  </Router>,
  document.getElementById('react-root'),
);
