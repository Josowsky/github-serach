import fetch from 'unfetch';

import { Repository } from 'types/models/Repository';

export interface FetchRepositoriesArgs {
  username: string;
}

export const fetchRepositories = async ({ username = '' }: FetchRepositoriesArgs): Promise<Repository[]> => {
  try {
    const response = await fetch(`https://api.github.com/search/repositories\?q\=user:${username}&sort=stars`);
    const { items, message } = await response.json();

    if (message) throw new Error(message);

    return items as Repository[];
  } catch (error) {
    throw new Error(error);
  }
};

export default {
  fetchRepositories,
};
