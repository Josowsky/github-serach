import fetch from 'unfetch';

import { User } from 'types/models/User';

export interface FetchUsersArgs {
  username: string;
}

export const fetchUsers = async ({ username = '' }: FetchUsersArgs): Promise<User[]> => {
  try {
    const response = await fetch(`https://api.github.com/search/users\?q\=${username}`);
    const { items, message } = await response.json();

    if (message) throw new Error(message);

    return items as User[];
  } catch (error) {
    throw new Error(error);
  }
};

export default {
  fetchUsers,
};
