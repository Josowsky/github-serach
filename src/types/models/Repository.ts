export interface Repository {
  id: number;
  full_name: string;
  html_url: string;
}
